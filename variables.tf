variable "app_name" {
  default = "kong"
}
variable "region" {
  default = "ap-southeast-2"
}
data "aws_availability_zones" "available" {}
