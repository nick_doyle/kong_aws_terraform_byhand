variable "identifier" {
  default     = "kong-rds"
  description = "Identifier for your DB"
}

variable "storage" {
  default     = "1"
  description = "Storage size in GB"
}

variable "engine" {
  default     = "postgres"
  description = "Engine type, example values mysql, postgres"
}

variable "engine_version" {
  description = "Engine version"

  default = {
    mysql    = "5.7.21"
    postgres = "9.6.8"
  }
}

variable "instance_class" {
  default     = "db.t2.micro"
  description = "Instance class"
}

variable "db_name" {
  default     = "kong"
  description = "db name"
}

variable "db_username" {
  default     = "kong"
  description = "User name"
}

variable "db_passwd" {
  description = "password, provide through your ENV variables"
}
