resource "aws_security_group" "kong_app_service" {
  name        = "kong_app"
  vpc_id      = "${aws_vpc.default.id}"

  # TODO 8001 admin from bastion only
  ingress {
    description = "kong inter-node comms, tcp and udp"
    from_port   = 7946
    to_port     = 7946
    protocol    = "-1"
    cidr_blocks = [
      "${var.subnet_cidr_public1}",
      "${var.subnet_cidr_public2}",
      "${var.subnet_cidr_public3}"
    ]
  }
  ingress {
    description = "kong proxy https"
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "kong proxy plain"
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description     = "allow all outbound"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
