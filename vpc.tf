resource "aws_vpc" "default" {
  cidr_block  = "${var.vpc_cidr}"
  tags {
    Name       = "kong ecs demo nick momenton"
  }
}

resource "aws_subnet" "public1" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_public1}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  tags {
    Name        = "ECS Public Subnet 1"
  }
}
resource "aws_subnet" "public2" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_public2}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  tags {
    Name        = "ECS Public Subnet 2"
  }
}
resource "aws_subnet" "public3" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_public3}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
  tags {
    Name        = "ECS Public Subnet 3"
  }
}

resource "aws_subnet" "private1" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_private1}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  tags {
    Name        = "ECS Private Subnet 1"
  }
}
resource "aws_subnet" "private2" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_private2}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  tags {
    Name        = "ECS Private Subnet 2"
  }
}
resource "aws_subnet" "private3" {
  vpc_id        = "${aws_vpc.default.id}"
  cidr_block    = "${var.subnet_cidr_private3}"
  availability_zone = "${data.aws_availability_zones.available.names[2]}"
  tags {
    Name        = "ECS Private Subnet 3"
  }
}

resource "aws_internet_gateway" "ecs" {
  vpc_id        = "${aws_vpc.default.id}"
  tags {
    Name        = "ECS IGW"
  }
}
resource "aws_route_table" "public" {
  vpc_id        = "${aws_vpc.default.id}"
  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = "${aws_internet_gateway.ecs.id}"
  }
  tags {
    Name        = "Public Route Table"
  }
}
resource "aws_main_route_table_association" "main" {
  vpc_id          = "${aws_vpc.default.id}"
  route_table_id  = "${aws_route_table.public.id}"
}
