resource "aws_ecs_service" "default" {
  name      = "${var.app_name}"
  cluster   = "${aws_ecs_cluster.main.id}"
  iam_role  = "${aws_iam_role.kong_service_role.arn}"
  task_definition   = "${aws_ecs_task_definition.default.arn}"
  # TODO load balancer
}
resource "aws_ecs_task_definition" "default" {
    family  = "${var.app_name}"
}
resource "aws_iam_role" "kong_service_role" {
  name               = "kong_container_instance_role"
}
