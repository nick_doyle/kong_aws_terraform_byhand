data "aws_ami" "ecs" {
  most_recent = true
  filter {
    name      = "owner-alias"
    values    = ["amazon"]
  }
  filter {
    name      = "name"
    values    = ["amzn-ami-*-amazon-ecs-optimized"]
  }
}
resource "aws_ecs_cluster" "main" {
  name            = "${var.ecs_cluster_name}"
}

data "template_file" "userdata" {
  template = "${file("templates/ecs-container-instance-userdata.tpl")}"
  vars {
    ecs_cluster_name = "${aws_ecs_cluster.main.name}"
  }
}

resource "aws_launch_configuration" "main" {
  name_prefix                 = "kong_cluster"
  image_id                    = "${data.aws_ami.ecs.id}"
  instance_type               = "${var.ecs_instance_type}"
  user_data                   = "${data.template_file.userdata.rendered}"
  key_name                    = "${var.ssh_key_name}"
  iam_instance_profile        = "${aws_iam_instance_profile.ecs.name}"
  security_groups             = ["${aws_security_group.allow_all.id}"]
  associate_public_ip_address = true
  lifecycle {
    create_before_destroy     = true
  }
}
resource "aws_autoscaling_group" "main" {
  launch_configuration  = "${aws_launch_configuration.main.id}"
  vpc_zone_identifier   = [
    "${aws_subnet.public1.id}",
    "${aws_subnet.public2.id}",
    "${aws_subnet.public3.id}"
  ]
  # TODO load_balancers
  name                  = "kong asg"
  max_size              = "${var.asg_size_max}"
  min_size              = "${var.asg_size_min}"
  desired_capacity      = "${var.asg_size_desired}"
  health_check_type     = "EC2"
  tag {
    key                 = "Name"
    value               = "kong-ecs"
    propagate_at_launch = true
  }

}
resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all"
  vpc_id      = "${aws_vpc.default.id}"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.app_name}AllowAll"
  }
}

output "ami_id" {
  value         = "${data.aws_ami.ecs.id}"
}
