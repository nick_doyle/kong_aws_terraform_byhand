variable "ecs_cluster_name" {
  default = "kong cluster"
}
variable "ssh_key_name" {
  default = "jk_sydney"
}
variable "ecs_instance_type" {
  default = "t2.micro"
}
variable "asg_size_min" {
  default = "1"
}
variable "asg_size_max" {
  default = "2"
}
variable "asg_size_desired" {
  default = "1"
}
