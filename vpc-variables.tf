variable "vpc_cidr" {
  default = "10.88.0.0/16"
}
variable "subnet_cidr_public1" {
  default = "10.88.0.0/24"
}
variable "subnet_cidr_public2" {
  default = "10.88.1.0/24"
}
variable "subnet_cidr_public3" {
  default = "10.88.2.0/24"
}
variable "subnet_cidr_private1" {
  default = "10.88.10.0/24"
}
variable "subnet_cidr_private2" {
  default = "10.88.11.0/24"
}
variable "subnet_cidr_private3" {
  default = "10.88.12.0/24"
}
